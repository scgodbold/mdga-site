import datetime
import time
import yaml

from tqdm import tqdm

import lib.fleaflicker
import lib.console as c

import lib.query
import lib.game
import lib.season
import lib.team
import lib.page

class SiteConfig(lib.page.Page):
    template = 'index.html'
    parent_path = '/'
    name = 'index.html'

    def __init__(self, config_file):
        with open(config_file, 'r') as f:
            data = yaml.load(f.read(), Loader=yaml.Loader)
        self.league_id = data['league_id']
        self.league_name = data['name']
        self.refresh_unfinished = data.get('refresh_unfinished', False)
        self.template_dir = data.get('template_dir', 'templates')
        self.data_dir = data.get('data_dir', 'data')
        self.build_dir = data.get('target', 'public')
        self.refresh_standings = data.get('refresh_standings', False)
        self.league_years = data['years']
        self.ff = lib.fleaflicker.FleaFlickerApi(self.league_id)
        self.build_time = datetime.datetime.utcnow().strftime('%a %b %d %H:%M:%S%z UTC')

        # Our data
        self.games = []
        self.teams = {}
        self.seasons = {}
        self.name_map = {}

        # Fetch all our data that way we can use it everywhere and unself refresh
        self.fetch_data()
        self._build_teams()
        self._build_seasons()

        self.pages = [
            self
        ]

        self.pages += self.seasons.values()
        self.pages += self.teams.values()

    @property
    def template_data(self):
        payload = {
            'updated_at': self.build_time,
            'league_name': self.league_name,
            'league_years': self.seasons.keys(),
            'teams': [(t.id, t.team_name) for t in self.teams.values()],
            'all_time_record': self._build_record(self.games).values(),
        }
        return payload


    def _build_teams(self):
        for g in tqdm(self.games, desc='Build Teams'):
            if g.home_team not in self.teams:
                self.teams[g.home_team] = lib.team.Team(g.home_team, self.build_dir, self.template_dir,
                                                        self.build_time, self.league_name)
            if g.away_team not in self.teams:
                self.teams[g.away_team] = lib.team.Team(g.away_team, self.build_dir, self.template_dir,
                                                        self.build_time, self.league_name)
            self.teams[g.home_team].add_game(g)
            self.teams[g.away_team].add_game(g)

        self.name_map = {t.id: t.team_name for t in self.teams.values()}

        # Update name map for team lookups
        for t in self.teams:
            self.teams[t].update_name_map(self.name_map)

    def _build_seasons(self):
        for y, y_config in tqdm(self.league_years.items(), desc='Build Seasons'):
            games = filter(lambda x: x.year == y, self.games)
            self.seasons[y] = lib.season.Season(y, y_config['weeks'], self.build_dir,
                                                self.template_dir, self.build_time,
                                                self.league_name, self.teams, self.name_map,
                                                y_config.get('divisions'))
            [self.seasons[y].add_game(g) for g in games]

    # {{{1 Get data
    def fetch_data(self):
        self._fetch_matchups()

    def _fetch_matchups(self):
        for year, _ in self.league_years.items():
            self._fetch_year_matchups(year)

    def _check_finished(self, data):
        #
        current_epoch = int(round(time.time() * 1000))
        if int(data['schedulePeriod']['low']['startEpochMilli']) > current_epoch:
            return False
        if not data['games'][0].get('isFinalScore', False):
            return False
        return True

    def _fetch_year_matchups(self, year):
        for week in tqdm(range(1, self.league_years[year].get('weeks', 16)+1), desc='Fetch weeks {}'.format(year)):
            data = self.ff.fetch_scoreboard(week, year)
            if not self._check_finished(data):
                if self.refresh_unfinished:
                    data = self.ff.fetch_scoreboard(week, year, refresh=True)
                    if not self._check_finished(data):
                        break
                else:
                    break
            self.games += [lib.game.Game(d, year, week, self.ff) for d in data['games']]
    # }}}

    def build(self):
        for page in tqdm(self.pages, desc='Render Pages'):
            page.render()
