import click

import lib.fleaflicker as fleaflicker
import lib.page as page

import config


@click.group()
def cli():
    pass


@click.command()
def build():
    site = config.SiteConfig('site.yml')
    site.build()


cli.add_command(build)


if __name__ == '__main__':
    cli()
