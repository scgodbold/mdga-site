import functools
import lib.game
from lib.page import Page


class Season(Page):
    template = 'season.html'
    parent_path = '/season'

    def __init__(self,
                 year,
                 weeks,
                 build_dir,
                 template_dir,
                 build_time,
                 league_name,
                 teams,
                 name_map,
                 divisions=None):

        self.build_dir = build_dir
        self.template_dir = template_dir
        self.build_time = build_time
        self.league_name = league_name
        self.name_map = name_map
        self.year = year
        self.name = '{}.html'.format(year)
        self.weeks = weeks
        self.divisions = divisions
        self.games = []
        self.teams = teams

    def add_game(self, game):
        self.games.append(game)

    def __repr__(self):
        return "Season({})".format(self.year)

    # {{{1 Properties
    @property
    def template_data(self):
        payload = {
            'updated_at': self.build_time,
            'league_name': self.league_name,
            'year': self.year,
            'season_record': self.regular_season_records,
            'completed': self.completed,
            'champion': self.champion,
            'use_divisions': self.use_divisions,
        }

        if self.use_divisions:
            payload['division_rankings'] = self.division_records

        return payload

    @property
    def completed(self):
        g = list(filter(lambda x: x.week==self.weeks, self.games))
        if len(g) > 0:
            if g[0].final:
                return True
        return False

    @property
    def use_divisions(self):
        return self.divisions is not None

    @property
    def playoff_record(self):
        return self._build_record(self.playoff_games())

    @property
    def champion(self):
        records = self.playoff_record
        for _, record in records.items():
            if record['losses'] == 0:
                return record['name']

    @property
    def division_records(self):
        record = {}

        #2 {{{2 special sort method
        def sort_method(x, y):
            # {{{3 Primary Regular season wins
            if x['wins'] > y['wins']:
                return -1
            if x['wins'] < y['wins']:
                return 1
            # }}}
            # {{{3 Tiebreak 1: Who has less losses
            if x['losses'] < y['losses']:
                return -1
            if x['losses'] > y['losses']:
                return 1
            # }}}
            # {{{3 Tiebreak 2: Head to Head record
            head2head_games = self.team_games(y['id'], self.team_games(x['id']))
            head2head_record = self._build_record(head2head_games)
            if head2head_record[x['id']]['wins'] > head2head_record[y['id']]['wins']:
                return -1
            if head2head_record[x['id']]['wins'] < head2head_record[y['id']]['wins']:
                return 1
            # }}}
            # {{{3 Point number Crunching
            x_games = self.team_games(x['id'])
            x_points = 0.0
            x_opp_points = 0.0
            for g in x_games:
                x_points += g.score(x['id'])
                x_opp_points += g.opp_score(x['id'])
            y_games = self.team_games(y['id'])
            y_points = 0.0
            y_opp_points = 0.0
            for g in y_games:
                y_points += g.score(y['id'])
                y_opp_points += g.opp_score(y['id'])
            # }}}
            # {{{3 Tiebreak 3: Points avg
            if x_points > y_points:
                return -1
            if x_points < y_points:
                return 1
            #}}}
            # {{{3 Tiebreak 4: Opponent Points
            if x_opp_points > y_opp_points:
                return -1
            if x_opp_points < y_opp_points:
                return 1
            # }}}
            return 0
        # }}}

        for name, teams in self.divisions.items():
            results = self._build_record(self.division_games(teams, self.regular_season()))
            div_record = [r for team_id, r in results.items() if team_id in teams]
            div_record.sort(key=functools.cmp_to_key(sort_method))
            record[name] = div_record
        return record

    @property
    def regular_season_records(self):
        record = self._build_record(self.regular_season())
        return record.values()

    # }}}


    # {{{1 Game filters
    def regular_season(self, games=None):
        if games is None:
            games = self.games
        return filter(lambda x: x.type == lib.game.Type.regular, games)

    def playoff_games(self, games=None):
        if games is None:
            games = self.games
        return filter(lambda x: x.type == lib.game.Type.playoff, games)

    def consolation_record(self, games=None):
        if games is None:
            games = self.games
        return filter(lambda x: x.type == lib.game.Type.consolation, games)

    def division_games(self, division_ids, games=None):
        filter_func = lambda x: x.home_team in division_ids or x.away_team in division_ids
        if games is None:
            games = self.games
        return filter(filter_func, games)

    def team_games(self, team_id, games=None):
        filter_func = lambda x: x.participated(team_id)
        if games is None:
            games = self.games
        return filter(filter_func, games)
    # }}}
