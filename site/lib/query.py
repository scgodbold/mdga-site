import functools
import time

def get_team_ids(site):
    data = site.ff.fetch_standings(list(site.league_years.keys())[-1])
    team_ids = []
    for division in data['divisions']:
        for team in division['teams']:
            team_ids.append(team['id'])
    return team_ids

def get_team_name(team_id, site):
    data = site.ff.fetch_standings(list(site.league_years.keys())[-1])
    for division in data['divisions']:
        for team in division['teams']:
            if team['id'] == team_id:
                break
    return team['name']

def is_season_finished(year, site):
    current_epoch = int(round(time.time() * 1000))
    for week in range(1, site.league_years[year].get('weeks', 16)+1):
        data = site.ff.fetch_scoreboard(week, year)
        if int(data['schedulePeriod']['low']['startEpochMilli']) > current_epoch:
            return False
    return True

def head2head_record(team, opp, years, site):
    record = {
        'wins': 0,
        'losses': 0,
        'ties': 0,
    }
    for year in years:
        resp = site.fetch_year_matchups(year)
        for g in resp:
            if g['away']['id'] in [team, opp] and g['home']['id'] in [team, opp]:
                if team == g['home']['id']:
                    result = g['homeResult']
                else:
                    result = g['awayResult']
                if result.lower() == 'win':
                    record['wins'] += 1
                elif result.lower() == 'lose':
                    record['losses'] += 1
                else:
                    record['ties'] += 1
    return record

def opps_faced(team, year, site):
    data = site.fetch_year_matchups(year)
    opps = []
    for g in data:
        if g['away']['id'] in [team, opp] and g['home']['id'] in [team, opp]:
            if g['away']['id'] == team and g['home']['id'] not in opps:
                    opps.append(g['home']['id'])
            elif g['home']['id'] not in ops and g['away']['id'] not in opps:
                    opps.append(g['away']['id'])
    return opps


def opp_wins(team, year, site):
    wlr = win_lose_record([year], site)
    opps = opps_faced(team, year, site)
    opp_wins = 0
    for opp in opps:
        opp_wins += wlr[opp]['wins']
    return opp_wins

def opp_points(team, year, site):
    team_points = team_points(team, [year], site)
    opps = opps_faced(team, year, site)
    points = 0
    for opp in opps:
        points += team_points[opp]['pointsFor']
    return points

def team_points(team, years, site):
    record = {}
    for year in years:
        resp = site.ff.fetch_standings(year)
        for division in data['division']:
            for team in division['teams']:
                team_id = team['id']
                if team_id not in record:
                    record[team_id] = {
                        'pointsFor': team['pointsFor']['value'],
                        'pointsAgainst': team['pointsAgainst']['value'],
                    }
                else:
                    record[team_id]['pointsFor'] += team['pointsFor']['value']
                    record[team_id]['pointsAgainst'] += team['pointsAgainst']['value']
    return record


def game_win_lose_record(games, site):
    record = {}
    for game in games:
        if game.home_team not in record:
            record[game.home_team] = {
            }


def win_lose_record(years, site):
    record = {}
    for y in years:
        data = site.ff.fetch_standings(y)
        for division in data['divisions']:
            for team in division['teams']:
                team_id = team['id']
                if team_id not in record:
                    record[team_id] = {
                        'wins': team['recordOverall'].get('wins', 0),
                        'losses': team['recordOverall'].get('losses', 0),
                        'ties': team['recordOverall'].get('ties', 0),
                        'name': get_team_name(team_id, site),
                    }
                else:
                    record[team_id]['wins'] += team['recordOverall'].get('wins', 0)
                    record[team_id]['losses'] += team['recordOverall'].get('losses', 0)
                    record[team_id]['ties'] += team['recordOverall'].get('ties', 0)
    return record

def season_team_record(team_id, year, site):
    data = site.ff.fetch_standings(year)
    for division in data['divisions']:
        for team in division['teams']:
            if team['id'] == team_id:
                break
    return team

def win_leader(years, site):
    record = win_lose_record(years, site)
    wins = []
    for _, r in record.items():
        wins.append((r['name'], r['wins']))
    wins.sort(key=lambda x: x[1], reverse=True)
    return wins

def win_percentage(years, site):
    record = win_lose_record(years, site)
    win_p_record = []
    for _, r in record.items():
        win_p_record.append((r['name'], float(r['wins']) / float(r['wins'] + r['losses'] + r['ties'])))
    win_p_record.sort(key=lambda x: x[1], reverse=True)
    return win_p_record

def teams(site):
    data = site.ff.fetch_standings(list(site.league_years.keys())[-1])
    teams = []
    for division in data['divisions']:
        for team in division['teams']:
            teams.append((team['id'], team['name']))
    teams.sort(key=lambda x: x[0])
    return teams

def season_champion(year, site):
    data = site.ff.fetch_standings(year)
    for division in data['divisions']:
        for team in division['teams']:
            if team['recordOverall']['rank'] == 1:
                break

    return get_team_name(team['id'], site)

def division_rank(members, year, site):
    records = []
    for member in members:
        records.append(season_team_record(member, year, site))

    #{{{1 Sorting method
    def sortMethod(x, y):
        # Primary: who has more wins
        if x['recordOverall'].get('wins', 0) > y['recordOverall'].get('wins', 0):
            return -1
        if x['recordOverall'].get('wins', 0) < y['recordOverall'].get('wins', 0):
            return 1

        # Tiebreak 1: head 2 head record
        h2h_record = head2head_record(x['id'], y['id'], [year], site)
        if h2h_record['wins'] > h2h_record['losses']:
            return -1
        if h2h_record['losses'] > h2h_record['wins']:
            return 1

        # Tiebreak 2: pointavg/game
        loses = x['recordOverall'].get('losses', 0)
        wins = x['recordOverall'].get('wins', 0)
        ties = x['recordOverall'].get('ties', 0)
        points_avg_x = x['pointsFor']['value'] / float(wins + loses + ties)
        loses = y['recordOverall'].get('losses', 0)
        wins = y['recordOverall'].get('wins', 0)
        ties = y['recordOverall'].get('ties', 0)
        points_avg_y = y['pointsFor']['value'] / float(wins + loses + ties)
        if points_avg_x > points_avg_y:
            return -1
        if points_avg_y > points_avg_x:
            return 1

        #Tiebreak 3: Opp wins
        x_oppwins = opp_wins(x['id'], year, site)
        y_oppwins = opp_wins(y['id'], year, site)
        if x_oppwins > y_oppwins:
            return -1
        if y_oppwins > x_oppwins:
            return 1

        #Tiebreak 4: Opp pints
        x_opppoints = opp_points(x['id'], year, site)
        y_opppoints = opp_points(y['id'], year, site)
        if x_opppoints > y_opppoints:
            return -1
        if y_opppoints > x_opppoints:
            return 1

        # Out of tiebreakers
        return 0
    # }}}}
    records.sort(key=functools.cmp_to_key(sortMethod))
    return records

def division_rankings(divisions, year, site):
    rankings = {}
    for name, members in divisions.items():
        rankings[name] = division_rank(members, year, site)
    return rankings

