from colorama import Fore, Style


def warn(msg):
    print(Fore.YELLOW + msg + Style.RESET_ALL)


def success(msg):
    print(Fore.GREEN + msg + Style.RESET_ALL)


def error(msg):
    print(Fore.RED + msg + Style.RESET_ALL)

def info(msg):
    print(Fore.RED + msg + Style.RESET_ALL)
