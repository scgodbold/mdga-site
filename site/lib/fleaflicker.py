import json
import os
import os.path
import requests

from lib.exceptions import FleaFlickerFetchError
import lib.console as console


class FleaFlickerApi(object):
    def __init__(self, league_id, sport='NFL', cache_dir='data'):
        self.league_id = league_id
        self.sport = sport
        self.cache_dir = '{path}/fleaflicker'.format(path=cache_dir)

        self._init_cache()

    def _init_cache(self):
        os.makedirs(self.cache_dir, exist_ok=True)

    def _get_url(self, url):
        resp = requests.get(url)
        resp.raise_for_status()
        if resp.status_code not in [200]:
            # console.error("Failed to fetch: {}".format(url))
            raise FleaFlickerFetchError('Unable to fetch url: {url}'.format(url=url))

        return resp.json()

    def _cache_data(self, data, path):
        cache_dirs = os.path.dirname(path)
        os.makedirs(cache_dirs, exist_ok=True)
        with open(path, 'w') as f:
            f.write(json.dumps(data, sort_keys=True, indent=2))

    def _cache_or_get(self, url, cache_key, refresh=False):
        cache_file = '{cache_dir}/{cache_key}.json'.format(cache_dir=self.cache_dir,
                                                           cache_key=cache_key)
        if os.path.exists(cache_file) and not refresh:
            with open(cache_file, 'r') as f:
                return json.load(f)

        data = self._get_url(url)
        self._cache_data(data, cache_file)

        return self._get_url(url)

    def fetch_scoreboard(self, week, year, refresh=False):
        cache_key = '{sport}/{league_id}/{year}/{week}/scoreboard'.format(sport=self.sport,
                                                                          league_id=self.league_id,
                                                                          year=year,
                                                                          week=week)
        URL = 'http://www.fleaflicker.com/api/FetchLeagueScoreboard?sport={sport}&' \
            'league_id={id}&scoring_period={week}&season={year}'
        return self._cache_or_get(URL.format(sport=self.sport,
                                             id=self.league_id,
                                             week=week,
                                             year=year),
                                  cache_key=cache_key,
                                  refresh=refresh)

    def fetch_standings(self, year, refresh=False):
        URL = 'http://www.fleaflicker.com/api/FetchLeagueStandings?sport={sport}&' \
              'league_id={id}&season={year}'
        cache_key = '{sport}/{league_id}/{year}/standings'.format(sport=self.sport,
                                                                  league_id=self.league_id,
                                                                  year=year)
        return self._cache_or_get(URL.format(sport=self.sport,
                                             id=self.league_id,
                                             year=year),
                                  cache_key=cache_key,
                                  refresh=refresh)

    def fetch_box_score(self, week, game_id, refresh=False):
        URL = 'http://www.fleaflicker.com/api/FetchLeagueBoxscore?sport={sport}&' \
              'league_id={leauge_id}&scoring_period={week}&fantasy_game_id={game_id}'
        cache_key = '{sport}/{league_id}/games/{game_id}'.format(sport=self.sport,
                                                                 league_id=self.league_id,
                                                                 game_id=game_id)
        return self._cache_or_get(URL.format(sport=self.sport,
                                             leauge_id=self.league_id,
                                             week=week,
                                             game_id=game_id),
                                  cache_key=cache_key,
                                  refresh=refresh)
