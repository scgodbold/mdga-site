import os
import os.path
import yaml
import lib.game

from jinja2 import Environment, FileSystemLoader


class Page(object):
    template = None
    template_dir = None
    games = []
    template_data = {}
    name_map = {}

    def __repr__(self):
        return 'Page({name}, {template})'.format(name=self.name, template=self.template)

    def _assert_parent_exists(self, path):
        os.makedirs(os.path.dirname(path), exist_ok=True)

    def render(self):
        if self.template is None or self.template_dir is None:
            raise NotImplementedError()
        env = Environment(loader=FileSystemLoader(self.template_dir))
        template = env.get_template(self.template)

        build_path = os.path.normpath('{build_dir}/{parent_path}/{name}'.format(build_dir=self.build_dir,
                                                                                parent_path=self.parent_path,
                                                                                name=self.name))
        self._assert_parent_exists(build_path)
        with open(build_path, 'w') as f:
            f.write(template.render(**self.template_data))

    def _build_record(self, games=None):
        record = {}
        if games is None:
            games = self.games
        for g in games:
            if g.home_team not in record:
                record[g.home_team] = {
                    'wins': 0,
                    'losses': 0,
                    'ties': 0,
                    'points_for': 0.0,
                    'points_against': 0.0,
                    'optimum_points': 0.0,
                    'id': g.home_team,
                }
            if g.away_team not in record:
                record[g.away_team] = {
                    'wins': 0,
                    'losses': 0,
                    'ties': 0,
                    'points_for': 0.0,
                    'points_against': 0.0,
                    'optimum_points': 0.0,
                    'id': g.away_team,
                }

            # Process Game Results
            record[g.home_team]['wins'] += 1 if g.home_result == lib.game.Result.win else 0
            record[g.home_team]['losses'] += 1 if g.home_result == lib.game.Result.lose else 0
            record[g.home_team]['ties'] += 1 if g.home_result == lib.game.Result.tie else 0
            record[g.away_team]['wins'] += 1 if g.away_result == lib.game.Result.win else 0
            record[g.away_team]['losses'] += 1 if g.away_result == lib.game.Result.lose else 0
            record[g.away_team]['ties'] += 1 if g.away_result == lib.game.Result.tie else 0

            # Process Points
            record[g.home_team]['points_for'] += g.home_score
            record[g.home_team]['points_against'] += g.away_score
            record[g.home_team]['optimum_points'] += g.home_optimum
            record[g.away_team]['points_for'] += g.away_optimum
            record[g.away_team]['optimum_points'] += g.home_optimum
            record[g.away_team]['points_against'] += g.home_score

        for r in record:
            points = record[r]['points_for']
            optimum_points = record[r]['optimum_points']
            wins = record[r]['wins']
            losses = record[r]['losses']
            ties = record[r]['ties']
            record[r]['name'] = self.name_map.get(r)
            record[r]['win_percentage'] = float(wins)/float(wins+losses+ties)
            if optimum_points != float(0):
                record[r]['optimum_point_percentage'] = float(points)/float(optimum_points)
            else:
                record[r]['optimum_point_percentage'] = None

        return record
