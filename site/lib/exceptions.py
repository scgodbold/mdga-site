class FleaFlickerFetchError(Exception):
    def init(self, message):
        super(FleaFlickerFetchError, self).__init__(message)
