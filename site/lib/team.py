import lib.game

from lib.page import Page


class Team(Page):
    template = 'team.html'
    parent_path = '/team/'

    def __init__(self, team_id, build_dir, template_dir, build_time, league_name, name_map=None):
        self.build_dir = build_dir
        self.template_dir  = template_dir
        self.name = '{}.html'.format(team_id)
        self.id = team_id
        self.team_names = {}
        self.seasons = {}
        self.regular_seasons = {}
        self.games = []
        self.latest = None
        self.build_time = build_time
        self.league_name = league_name
        self.name_map = name_map
        self.years = []
        self.opps = []

    def update_name_map(self, name_map):
        self.name_map = name_map

    @property
    def template_data(self):
        return {
            'updated_at': self.build_time,
            'league_name': self.league_name,
            'team_name': self.team_name,
            'record': self._build_record(self.games, self.id),
            'vsrecord': self._build_vs_record().values(),
            'year_record': self._build_year_record(),
        }

    @property
    def team_name(self):
        return self.team_names.get(self.latest)

    def add_game(self, g):
        self.games.append(g)
        y = g.year
        if self.latest is None:
            self.latest = y
        elif self.latest < y:
            self.latest = y
        if y not in self.team_names:
            self.team_names[y] = g.get_name(self.id)
        if y not in self.years:
            self.years.append(y)
        if g.opp_id(self.id) not in self.opps:
            self.opps.append(g.opp_id(self.id))

    def points_for(self, games=None):
        if games is None:
            games = self.games
        score = 0.0
        for game in games:
            score += game.score(team_id)
        return score

    def points_against(self, games=None):
        if games is None:
            games = self.games
        score = 0.0
        for game in games:
            score += game.opp_score(team_id)
        return score

    def _build_record(self, games, target):
        r = super(Team, self)._build_record(games)
        return r[target]

    def _build_vs_record(self):
        records = {}
        for opp_id in self.opps:
            r = self._build_record(self.opposition(opp_id), opp_id)
            # Records are built from the opposition POV
            # In this case we want that flipped because its all about
            # The team in question
            wins = r['wins']
            losses = r['losses']
            r['wins'] = losses
            r['losses'] = wins
            records[opp_id] = r

        return records

    def _build_year_record(self):
        records = {}
        for y in self.years:
            records[y] = self._build_record(self.year(y), self.id)
        return records


    #{{{1 Game filters
    def wins(self, games=None):
        filter_func = lambda x: x.result(self.id) == lib.game.Result.win
        if games is None:
            games = self.games
        return filter(filter_func, games)

    def loses(self, games=None):
        filter_func = lambda x: x.result(self.id) == lib.game.Result.lose
        if games is None:
            games = self.games
        return filter(filter_func, games)

    def ties(self, games=None):
        filter_func = lambda x: x.result(self.id) == lib.game.Result.tie
        if games is None:
            games = self.games
        return filter(filter_func, games)

    def year(self, year, games=None):
        filter_func = lambda x: x.year == year
        if games is None:
            games = self.games
        return filter(filter_func, games)

    def post_season(self, games=None):
        filter_func = lambda x: x.post_season
        if games is None:
            games = self.games
        return filter(filter_func, games)

    def playoffs(self, games=None):
        filter_func = lambda x: x.type == lib.game.Type.playoff
        if games is None:
            games = self.games
        return filter(filter_func, games)

    def consolation(self, games=None):
        filter_func = lambda x: x.type == lib.game.Type.consolation
        if games is None:
            games = self.games
        return filter(filter_func, games)

    def opposition(self, opp_id, games=None):
        filter_func = lambda x: x.participated(opp_id)
        if games is None:
            games = self.games
        return filter(filter_func, games)
    #}}}
