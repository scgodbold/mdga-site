import time
from enum import Enum


class Result(Enum):
    lose = 0
    win = 1
    tie = 2


class Type(Enum):
    regular = 0
    playoff = 1
    consolation = 2


class Game(object):
    def __init__(self, game_data, year, week, ff_api):
        self.id = game_data['id']
        # Time Info
        self.year = year
        self.week = week
        self.final = game_data.get('isFinalScore', False)

        # Teams
        self.home_team = game_data['home']['id']
        self.away_team = game_data['away']['id']

        # Current Team Names
        self.home_name = game_data['home']['name']
        self.away_name = game_data['away']['name']

        # Scores
        self.home_score = game_data['homeScore']['score']['value']
        self.away_score = game_data['awayScore']['score']['value']

        # Results
        self.home_result = Result[game_data['homeResult'].lower()]
        self.away_result = Result[game_data['awayResult'].lower()]

        # Default game type
        self.type = Type.regular

        # Update type based on data
        playoffs = game_data.get('isPlayoffs', False)
        consolation = game_data.get('isConsolation', False)
        if playoffs:
            self.type = Type.playoff
        elif consolation:
            self.type = Type.consolation

        refresh=False
        if not self.final:
            refresh=True
        resp = ff_api.fetch_box_score(week, game_data['id'], refresh=refresh)

        # Optimum points
        self.home_optimum = resp['pointsHome']['total']['optimum']['value']
        self.away_optimum = resp['pointsAway']['total']['optimum']['value']

        if self.home_optimum < self.home_score:
            self.home_optimum = self.home_score
        if self.away_optimum < self.away_score:
            self.away_optimum = self.away_score

    @property
    def post_season(self):
        return self.type in [Type.playoff, Type.consolation]

    def participated(self, team_id):
        return team_id in [self.home_team, self.away_team]

    def get_name(self, team_id):
        if self.participated(team_id):
            return self.home_name if team_id == self.home_team else self.away_name
        return None

    def result(self, team_id):
        if self.participated(team_id):
            return self.home_result if team_id == self.home_team else self.away_result
        return None

    def opp_result(self, team_id):
        if self.participated(team_id):
            return self.away_result if team_id == self.home_team else self.home_result
        return None

    def score(self, team_id):
        if self.participated(team_id):
            return self.home_result if team_id == self.home_team else self.away_result
        return None

    def opp_score(self, team_id):
        if self.participated(team_id):
            return self.away_result if team_id == self.home_team else self.home_result
        return None

    def opp_id(self, team_id):
        if self.participated(team_id):
            return self.away_team if team_id == self.home_team else self.home_team
        return None

    def __repr__(self):
        return "{y}-{w}: {home} v {away}".format(y=self.year, w=self.week,
                                                 home=self.home_team,
                                                 away=self.away_team,
                                                )
