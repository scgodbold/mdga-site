.PHONY: all

all: statics build

statics: node_modules
	./node_modules/.bin/sass ui/app:www/public/static/css

build: venv
	./venv/bin/python site/cli.py build

venv:
	python3 -m venv venv
	./venv/bin/pip install -r requirements.txt

node_modules:
	npm install

start:
	docker-compose up -d

stop:
	docker-compose down

clean:
	rm -rf ./venv
	rm -rf ./node_modules
	rm -rf ./www/public
